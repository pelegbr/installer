#!/bin/bash
#Elevate the script to run on root if not already.
if [ $EUID != 0 ]; then
	if [[ -t 1 ]]; then
		sudo "$0" "$@"
	else
		exec 1>output_file
		gksu "$0" "$@"
	fi
	exit
fi

#The errors encontered by the intaller.
ERRORS=0
PROGRASS=0

#Initializes the installer display.
function init {
	echo ___________________________________________________________________________________________________________
	echo
	echo ___________________________________________________________________________________________________________
	echo -----------------------------------------------------------------------------------------------------------
	echo
	echo -----------------------------------------------------------------------------------------------------------
	echo -n 0%[____________________________________________________________________________________________________]
}

#This function adds a new stage to the installer with the name of the first parameter.
function phase {
	update_text 5 "phase: $1"
#The steps of this phase.
	STEPS=()
#The names of the steps in this phase
	NAMES=()
#The current executing step
	CUR_STEP=0
}

#This function adds a step to the current phase
#parameters:
#$1=name of the step
#$2=percentage progress for this step.
#$3+=the commands to execute.
function step {
#If the phase contains atleast two steps add text one line from the end.
	if [ ${#STEPS[@]} -ge 2 ]; then
		update_text 1 "\t$1[waiting]"
	else
#Else add to the text to the 2 - STEP.Length line from the end.
		update_text $[2 - ${#STEPS[@]}] "\t$1[waiting]"
	fi
#Saves the information given to command in arr, arr's layout is {percentage, commands}.
	local arr=("$2")
	local args=("$@")
#Add all commands to arr.
	for arg in "${args[@]:2}"; do
		arr+=" '$arg'"
	done
#Add the name and information to the step's information holders(STEPS and NAMES)
	STEPS+=("$arr")
	NAMES+=("$1")
}

#This function starts a phase after it's been constructed.
#There are no parameters.
function phase_start {
	#Check if a phase has been constructed.
	if [ -z $CUR_STEP ] || [ $CUR_STEP -ge ${#STEPS[*]} ]; then 
		>&2 echo -e \n error: Can not start a phase without a phase declaration.
	fi
#Update display
	echo -en "\r\033[0K\r"
	cur_pos 1A
	echo -----------------------------------------------------------------------------------------------------------
#Draw prograss.
	draw_prograss
#Execute all commands.
	for step in "${STEPS[@]}"; do
#Find the information of the current step.
		local arr=($step)
#Start the step.
		start_step "${NAMES[$CUR_STEP]}" ${arr[@]:1}
#Add progress.
		step_done ${arr[0]} "${NAMES[$CUR_STEP]}"
	done
#If the installer is done
	if [ $PROGRASS -ge 100 ]; then 
	return 0
	fi
#Delete old display
	for i in `seq 0 $[${#STEPS[@]} - 2]`; do
		echo -en "\r\033[0K\r"
		cur_pos 1A
	done
}

#This function starts a phase
#parameters:
#$1: the name of the current step.
#$2: the information of the step.
function start_step {
#Keeps records on the last status
	LAST_EXIT=0
#Update the text from waiting to running.
	echo -en "\033[s"
	cur_pos $[2 + ${#STEPS[@]} - CUR_STEP - 1]A
	echo -en "\033[0K\r"
	echo -en "\t$1[running]"
	
#The parameters of the function.
	local arr=("$@")
#Adds text animation.
	open_spinner &
#The commands to run
	eval local oarr=("${arr[@]:1}")
#Run all the commands in the step.
	for com in "${oarr[@]}"; do
		eval "$com"
#If encountered an error add to ERRORS and LAST_EXIT=1
		if [ $? == 1 ]; then
			LAST_EXIT=1
			let ERRORS++
		fi
	done
	#LAST_EXIT=$?
#Stop the Animation
	kill "$!"
#Reset the cursor's position to the end of the text.
	cur_pos $[2 + ${#STEPS[@]} - CUR_STEP - 1]B
}

#This function handles what happens when a step gets done.
#parameters:
#$1: percentage of completion for this step.
#$2: the name of the step.
function step_done {
#Check whether errors occured and reports it.
	if [ $LAST_EXIT != 0 ]; then
		local STATE=failed
	else
		local STATE=done
	fi
#Set the cursor's position to the stage's line
	cur_pos $[1 + ${#STEPS[@]} - CUR_STEP]A
	echo -en "\033[0K\r"
#Updates the step's displayed state.
	echo -en "\t$2[${STATE}]"
#Increase prograss(only the variable
	let PROGRASS+=$1
#Reset the cursor's position to the end of the text.
	cur_pos $[2 + ${#STEPS[@]} - CUR_STEP - 1]B
#Update the displayed prograss
	draw_prograss
#Goto the next step.
	let CUR_STEP++
}

#This function updates the indicator of prograss.
#no parameters
function draw_prograss {
#Delete the current line.
	echo -en "\r"
#Print the percentage of prograss and the start of the prograss indicator.
	echo -n "$PROGRASS%|"
#Print the indicator(bar) of prograss
	for i in {0..99}; do
#If prograss is bigger than i add ▓ else add _ for missign prograss
		if [ $i -lt $PROGRASS ]; then echo -n "▓"
		else echo -n "_"
		fi
	done
#Draw the end of the indicator.
	echo -n "|"
}

#This function changes the line of the cursor.
#parameters:
#$1 the lines to go up.
function cur_pos {
	echo -en "\r"
	echo -en "\033[$1"
}

#This function updates a text in a line.
#parameters:
#$1: the nunmber of lines to go up for the text.
#$2: the text update.
function update_text {
	pos=$1
#Go back lines
	cur_pos ${pos}A
#Delete line
	echo -en "\033[0K\r"
#Add the new line.
	echo -e $2
#Reset the cursor's position to the end of the text.
	cur_pos $[pos -1]B
}

#This function animates an in prograss step.
#no parameters
function open_spinner {
	echo -n "   "
#While true
	while [ : ]; do
#Delete the last text printed and add the apropiate text instead.
#Than wait 0.5 seconds and do it again.
		echo -ne "\b\b\b   \b\b\b"
		sleep 0.5
		echo -ne ".  \b\b"
		sleep 0.5
		echo -ne "\b.. \b"
		sleep 0.5
		echo -ne "\b\b..."
		sleep 0.5
	done
}

#This function cleans up any spillover that might occur if the installer ends abruptly
#no parameters
function grace_close {
#If the cursor isn't in the right place, reset it.
	if [ ! -z $CUR_STEP ] && [ ! $CUR_STEP -ge ${#STEPS[*]} ]; then 
		echo -en "\033[u"
	fi
#Stop the animation.
	kill "$!"
#Close the process.
	exit
}

#Handle a close signal.
trap "grace_close" SIGKILL SIGINT
trap "kill \"$!\"" SIGTERM SIGSTOP

#This function attempts to find the index of an object inside an array.
#parameters:
#$1: the array.
#$2: what to search for.
#$3: the place to store the result.
function indexOf {
	local arr=$1
#if there's no place to store the result use default OUT parameter.
	if [ ! $3 ]; then local ret=OUT
	else 
		local ret=$3
	fi
#Iterate over all the array and search for an object matching $2.
	for i in "${!arr[*]}"; do
		if [[ "${arr[$i]}" == "$2" ]]; then
			eval $ret="$i"
#If there is store the index to $ret and return 0(no error)
			return 0
		fi
	done
#else return 1(error, not found).
	return 1
}

#This is a convenience function for executing a command in installer friendly manner(No stdout, err, go to log, silent, etc'...)
#parameters:
#$1 the command.
function install {
	eval $1 $INSTALL_PARAMS $OUTS
}

#This is a convenience function for executing a command to the log.
#parameters:
#$1 the command.
function redirect {
	eval $1 $OUTS
}
#Log logic
exec 2>${log_date}_install.err
log_date="$(date +%Y-%m-%d-%H)"

#Silent mode:
indexOf "$@" "--silent" 

#Decide where stdout and stderr go.
if [ $? == 0 ]; then 
OUTS="1>>/dev/null 2>>${log_date}_install.err"
else
OUTS="1>>${log_date}_install.log 2>>${log_date}_install.err"
fi

#Install parameters to be installer friendly(Applies only to apt-get)
INSTALL_PARAMS="-y --allow-unauthenticated"

#initialization of installer-ui
init

##################################################Here is an example of usage of the installer api###############################################

#first phase
phase setup

step "Updating cache and libraries" 6 "install \"apt-get update\"" "install \"apt-get upgrade\""

step "Install Cuda and Cudann" 8 cuda
function cuda {
#	redirect "dpkg -i cuda-repo-ubuntu1604-9-1-local_9.1.85-1_amd64.deb"
#	redirect "apt-key add /var/cuda-repo-9-1-local/7fa2af80.pub"
	redirect "dpkg -i cuda-repo-ubuntu1604_9.0.176-1_amd64.deb"
	redirect "apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub -y"
	install "apt-get update"
	install "apt-get install cuda"
	echo "export PATH=\"/usr/local/cuda/bin:${PATH}\" > ~/.bash_profile
	echo export LD_LIBRARY_PATH=\"/usr/local/cuda/lib64:$LD_LIBRARY_PATH\" > ~/.bash_profile
	echo export LD_LIBRARY_PATH=\"/usr/local/cuda/extras/CUPTI/lib64:${LD_LIBRARY_PATH}\"" > ~/.bash_profile
	redirect "dpkg -i libcudnn7_7.1.3.16-1+cuda9.1_amd64.deb"
}

step "Installing Graphics driver:" 8 "install \"apt-get purge nvidia*\"" "redirect \"add-apt-repository ppa:graphics-drivers/ppa -y\"" "install \"apt-get update\"" "install \"apt-get install nvidia-390\""
phase_start

phase install-deps
TS=4
if [ ! "$(command -v python)" ] || [ ! "$(command -v pip)" ]
then
step "Installing Python and pip" 2 "install \"apt-get install python-pip python-dev\""
else let TS+=2
fi

step "Installing Tensorflow" $TS "redirect \"pip install tensorflow-1.8.0-cp27-cp27mu-linux_x86_64.whl\""

step "Installing python dependecies" 6 "redirect \"pip install Jinja2 redis Werkzeug\"" \
									   "redirect \"python -mpip install matplotlib\""   \
									   "redirect \"pip install requests\""              \
									   "redirect \"pip install scikit-image\""          \
									   "install \"apt-get install python-tk\""          \
									   "redirect \"pip install sklearn\"" "redirect \"pip install traitlets\""

step "Downloading and installing openCV" 12 ocv

function ocv {
	#deps
	install "apt-get install build-essential"
	install "apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev"
	install "apt-get install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev"
	#donwload openCV.
	a=$(pwd)
	mkdir -p ~/dev
	cd ~/dev
#	redirect "git clone --branch 3.4 https://github.com/opencv/opencv"
	redirect "cp ${a}/opencv/* opencv"
	cd opencv
	#compile
	mkdir -p build
	cd build
	redirect "cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local .."
	redirect make
	#install
	redirect "make install"
	
	#Return to running path.
	cd $a

}
phase_start
phase "install-apps&services"

step "Installing product1" 6 "redirect \"mkdir -p /product1\"" "redirect \"cp -r ./product1/* /product1/\""

step "Installing product2" 6 "redirect \"mkdir -p /product2\"" "redirect \"cp -r ./product2/* /product2/\""

step "Installing product3" 6 "redirect \"mkdir -p /product3\"" "redirect \"cp -r ./product3/* /product3/\""

step "Installing product4" 6 "redirect \"mkdir -p /product4/\"" "redirect \"cp -r ./product4/* /product4/\"" "redirect \"cp product4.sh /usr/bin/\""

step "Installing product5" 2 "redirect \"mkdir -p /product5\"" "redirect \"cp -r ./product4/* /product5/\""

step "Installing product6" 6 "install \"apt-get install p7zip-full\"" "redirect \"7z x -o./product6 product6.7z\""  "chmod +x ./product6_install.sh" "redirect ./product6_install.sh -y"

step "installing integrations" 6 "redirect \"7z x -o./integrations integrations.7z -y\"" "redirect \"cp -r ./integrations/* /integrations/\"" "rm -r ./integrations"

step configurating-apps 6 conf_apps
function conf_apps {
	redirect "mkdir -p ~/.aws/"
	redirect "cp credentials ~/.aws/credentials"
	chmod -R 777 /product*
	chmod +x /product*
	chmod +x  /usr/bin/product4.sh
	chmod +x  /usr/bin/product6.sh
	
	redirect "product7 -service install"
	redirect "product7 -service start"
}

phase_start
phase "install-rc&java"
step "Installing Remote Connection apps" 5 "chmod +x ConnectWiseControl.ClientSetup.sh" "redirect \"bash ConnectWiseControl.ClientSetup.sh\"" "install \"apt-get install openssh-server\""

step "Installing java" 5 "redirect \"add-apt-repository ppa:linuxuprising/java -y\"" "install \"apt-get update\"" "echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections" "echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections" "install \"apt-get install oracle-java10-installer\""
phase_start
echo
if [ $ERRORS != 0 ]; then
	echo "Finished installing with $ERRORS errors."
else
	echo "Finsihed installing successfully."
fi
